package internal

import (
	"errors"
	"fmt"
	"log"
	"sync"
	"time"

	"gitlab.com/andrewvwebber/tinyurl/pkg/entities"
)

type FaultInjector func() error

const KVSTORE_KEYEXISTS = "key already exists"
const KVSTORE_KEYNOTFOUND = "key not found - %s"

type KVStoreStat struct {
	Value   int64
	Expires time.Time
}

type KVStore struct {
	lock          *sync.Mutex
	M             map[string]entities.ShortURL
	S             map[string]KVStoreStat
	FaultInjector FaultInjector
}

func NewKVStore(f FaultInjector) *KVStore {
	return &KVStore{
		lock:          &sync.Mutex{},
		M:             make(map[string]entities.ShortURL),
		S:             make(map[string]KVStoreStat),
		FaultInjector: f,
	}
}

func (s *KVStore) Insert(key string, shortURL entities.ShortURL) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	if s.FaultInjector != nil {
		if err := s.FaultInjector(); err != nil {
			return err
		}
	}

	if _, ok := s.M[key]; ok {
		return errors.New(KVSTORE_KEYEXISTS)
	}

	s.M[key] = shortURL
	log.Printf("insert %s", key)

	return nil
}

func (s *KVStore) URL(key string) (entities.ShortURL, error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	if s.FaultInjector != nil {
		if err := s.FaultInjector(); err != nil {
			return entities.ShortURL{}, err
		}
	}

	v, ok := s.M[key]
	if !ok {
		return entities.ShortURL{}, fmt.Errorf(KVSTORE_KEYNOTFOUND, key)
	}

	return v, nil
}

func (s *KVStore) IsShortURLExistsError(err error) bool {
	return err.Error() == KVSTORE_KEYEXISTS
}

func (s *KVStore) IncrStat(key string, expires uint32) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	c, ok := s.S[key]
	if !ok {
		c = KVStoreStat{}
	}

	c.Expires = time.Now().Add(time.Duration(expires) * time.Second)
	c.Value++
	s.S[key] = c
	return nil
}

func (s *KVStore) Stat(key string) (int64, error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	c, ok := s.S[key]
	if !ok {
		return 0, errors.New(KVSTORE_KEYNOTFOUND)
	}

	return c.Value, nil
}
