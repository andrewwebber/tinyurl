package usescases

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/andrewvwebber/tinyurl/pkg/entities"
	"github.com/rs/xid"
)

const maxLength = 21

type ShortURLRepository interface {
	Insert(key string, shortURL entities.ShortURL) error
	URL(key string) (entities.ShortURL, error)
	IsShortURLExistsError(err error) bool
	IncrStat(key string, expires uint32) error
	Stat(key string) (int64, error)
}

type URLShortener func(url string) string

func XIDURLShortener(url string) string {
	return xid.New().String()
}

func NewTinyURL(baseURL string, r ShortURLRepository, s URLShortener) TinyURL {
	return TinyURL{
		baseURL:    baseURL,
		repository: r,
		shortener:  s,
	}
}

type TinyURL struct {
	baseURL    string
	shortener  URLShortener
	repository ShortURLRepository
}

func (t *TinyURL) ShortenURL(url string) (entities.ShortURL, error) {
	result := entities.ShortURL{URL: url}

	var err error
	for retryCount := 0; retryCount < 5; retryCount++ {
		short := t.shortener(url)
		result.Short = short
		result.URL = url
		if err = t.repository.Insert(short, result); err != nil {
			if t.repository.IsShortURLExistsError(err) {
				continue
			}

			return result, err
		}

		result.Short = fmt.Sprintf("%s/%s", t.baseURL, result.Short)
		break
	}

	return result, err
}

func (t *TinyURL) URL(shortURL string) (string, error) {
	var result string
	shortURL = strings.Replace(shortURL, t.baseURL+"/", "", 1)
	if len(shortURL) > maxLength {
		return result, fmt.Errorf("invalid shorturl length %d", len(shortURL))
	}

	e, err := t.repository.URL(shortURL)
	if err != nil {
		return result, err
	}

	go incrementURLStats(e.Short, t.repository)

	result = e.URL
	return result, nil
}

type URLStats struct {
	Global int64
	Weekly int64
	Daily  int64
}

func (t *TinyURL) Stats(shortURL string) (URLStats, error) {
	var result URLStats
	shortURL = strings.Replace(shortURL, t.baseURL+"/", "", 1)
	if len(shortURL) > maxLength {
		return result, fmt.Errorf("invalid shorturl length %d - %s", len(shortURL), shortURL)
	}

	return urlStats(shortURL, t.repository)

}
func urlStats(url string, r ShortURLRepository) (URLStats, error) {
	var globalCount, weeklyCount, dailyCount int64

	var err error
	globalUsageKey := fmt.Sprintf("global:%s", url)
	globalCount, err = r.Stat(globalUsageKey)
	if err != nil {
		return URLStats{}, err
	}

	for i := 0; i < 7; i++ {
		dailyKey := fmt.Sprintf("daily:%d:%s", i, url)
		dailyCount, err := r.Stat(dailyKey)
		if err != nil {
			//TODO check for key not found
			continue
		}

		weeklyCount += dailyCount
	}

	for i := 0; i < 24; i++ {
		hourlyKey := fmt.Sprintf("hour:%d:%s", i, url)
		hourlyCount, err := r.Stat(hourlyKey)
		if err != nil {
			//TODO check for key not found
			continue
		}

		dailyCount += hourlyCount
	}

	return URLStats{Global: globalCount, Weekly: weeklyCount, Daily: dailyCount}, nil
}

func incrementURLStats(url string, r ShortURLRepository) {
	globalUsageKey := fmt.Sprintf("global:%s", url)
	dailyKey := fmt.Sprintf("daily:%d:%s", time.Now().Weekday(), url)
	hourlyKey := fmt.Sprintf("hour:%d:%s", time.Now().Hour(), url)
	_ = r.IncrStat(globalUsageKey, 0)
	_ = r.IncrStat(dailyKey, 604800)
	_ = r.IncrStat(hourlyKey, 86400)
}
