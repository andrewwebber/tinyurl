# TinyURL

## TL;DR;

Start server
```bash
make db build test
./bin/server
```

Use CURL to request a shortened URL
```bash
curl -X POST http://localhost:8080/client/v1/tinyurl/shorten -d 'url=http://localhost:8080/foo/bar'
```

Use CURL to request a redirect for the shortened URL
```bash
curl -v http://localhost:8080/bohntrk96a84vldf20h0
```

Use CURL to request stats for the shortened URL
```bash
curl -v http://localhost:8080/bohntrk96a84vldf20h0/stats
{"Count":12,"Week":12,"Last24Hours":11}
```

If I had more time
- This project structure is an experiment and novice approach to ["Clean Architecture"](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) in Golang. I still have much to learn.
- Switch to scylladb or a non "open core" NoSQL database.
- Prometheus Monitoring Stats, Jaeger metrics, etc.
- Health Checks
- Input request validation
- Fault Injection testing
- Linting scripts
- Logging
- Swagger generated API documentation

## Introduction

TinyURL is an sample application attempting to emulate a URL shortener. 

The application must support millions of short urls.


## Design

### Overall Design
The approach was to take advantage of advancements of [globally unique ids](https://github.com/rs/xid). This is due to initial concerns of global distribution. If the service would be deployed globally then concerns around globally synchronized state start to surface. Using a globally unique id in combination of an eventually consistent database should reduce the likelihood of a collision when replicating state between data centers.

In order to further address these concerns my assumption would be that either of the two options would be exercised.
- Write operations occur on a primary data center where write operations are replicated to read only replicas existing globally.
- Write operations can occur in each data center. A mesh of database nodes in different data centers receive replication from each other.

Using Geolocation based routing a user generating a URL should be working the nearest data center and read operations would be immediately valid. Eventually write operations would replicate globally.

### Cloudflare Design
My initial design was to utilize Cloudflare Workers specifically Cloudflare Workers KV.
This would have the advantage of taking advantage of Cloudflare global distribution of not only Workers but global state.
Unfortunately KV does not have a free-tier so I moved to another solution.

```mermaid
sequenceDiagram
    Client-->>CFWorkers: create short URL
    CFWorkers-->>CFWorkers: generate short URL
    CFWorkers-->>CFKV: write URL for short URL key
    CFWorkers-->>Client: OK
    Client-->>CFWorkers: lookup short URL
    CFWorkers-->>CFKV: lookup short URL by key
    CFKV-->>CFWorkers: original URL
    CFWorkers-->>Client: original URL
```

### Current Design

The current approach is a simple stateless service that writes shortened URLs into a Horizontally scaled NoSQL database (Memcached + Persistent Storage = Couchbase Server).

Having a NoSQL allows for horizontal scaling on in the local data center. Most NoSQL databases also support cross data center replication.

Using a database means the service will be able to survive application server reboots.



```mermaid
sequenceDiagram
    Client-->>Golang Service: create short URL
    Golang Service-->>Golang Service: generate short URL
    Golang Service-->>Couchbase Cluster: write URL for short URL key
    Couchbase Cluster-->>Global Datacenters: replicate
    Golang Service-->>Client: OK
    Client-->>Golang Service: lookup short URL
    Golang Service-->>Couchbase Cluster: lookup short URL by key
    Couchbase Cluster-->>Golang Service: original URL
    Golang Service-->>Client: original URL
```

## Getting Started

### Pre-requisites 

- [Golang SDK](https://golang.org/)
- [Docker](https://get.docker.com/)
- Make
- curl


### Local build and test

#### Database
Start a local Couchbase node using the script below.

```bash
make db
```

Once started you can open the web UI by going to [http://localhost:8090](http://localhost:8091)
The following default credentials will get you in. From there you will be able to explore the only single "bucket" called tinyurl.
- Username: Administrator
- Password: password

#### Build and Test

The project includes a make file for build and testing.

I hope to add [Golang CI - Lint](https://github.com/golangci/golangci-lint) to the make file

```bash
make build test
```
